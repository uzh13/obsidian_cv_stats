# Общее количество по категориям
```dataview
LIST WITHOUT ID Status + ": " + length(rows)
FROM "Resumes"
GROUP BY Status
SORT BY DESC
```
```dataview
LIST WITHOUT ID "Всего: " + length(rows)
from "Resumes"
GROUP BY hello
```

# Конверсия
```dataviewjs
let allNotes = dv.pages('"Resumes"');
let doneCount = 0;
let totalCount = 0;

for (let note of allNotes) {	
	let isOk = (
		note.Status == "Answer"
		 || note.Status == "Interview"
		 || note.Status == "Done"
	);
	if (isOk){
		doneCount++;
	}
	
	let isOurCase = (note.Status == "Send" || isOk);
	if (!isOurCase) {
		continue;
	}
	
	totalCount++;
}

let donePercentage = (doneCount / totalCount) * 100;
let output = "## В отклики " + donePercentage.toFixed(2) + "%"
dv.paragraph(output);
```

```dataviewjs
let allNotes = dv.pages('"Resumes"');
let doneCount = 0;
let totalCount = 0;

for (let note of allNotes) {	
	let isOk = (
		note.Status == "Done"
	);
	if (isOk){
		doneCount++;
	}
	
	let isOurCase = (
		note.Status == "Send"
		 || note.Status == "Answer"
		 || note.Status == "Interview"
		 || isOk
	);
	if (!isOurCase) {
		continue;
	}
	
	totalCount++;
}

let donePercentage = (doneCount / totalCount) * 100;
let output = "## В собеседования "
+ donePercentage.toFixed(2)
+ "%";
dv.paragraph(output);
```


# Рассылка резюме
```dataview
CALENDAR SendDate
FROM "Resumes"
```
```dataviewjs
let path = '"Resumes"';
let fieldName = "SendDate"
let allNotes = dv.pages(path);
let years = {};
let monthNames = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"];

for (let note of allNotes) {
    let date = new Date(note[fieldName]);
    let year = date.getFullYear();
    let month = monthNames[date.getMonth()];
    let day = date.getDate();

    if (!years[year]) years[year] = { count: 0, months: {} };
    if (!years[year]["months"][month]) years[year]["months"][month] = { count: 0, days: {} };
    if (!years[year]["months"][month]["days"][day]) years[year]["months"][month]["days"][day] = [];

    years[year]["count"]++;
    years[year]["months"][month]["count"]++;
    years[year]["months"][month]["days"][day].push(note);
}

let output = "";
for (let year in years) {
    output += "## "
     + year 
     + "<sup>"
     + years[year]["count"]
     + "</sup>" 
     + "\n";
    for (let month in years[year]["months"]) {
        output += " - **"
         + month
         + "**" 
         + "<sup>" 
         + years[year]["months"][month]["count"] 
         + "</sup>"
         + "\n";
    }
}

dv.paragraph(output);
```

# Напомнить о себе
## Отправленные
```dataview
LIST date(SendDate)
FROM "Resumes"
WHERE Status = "Send"
AND Ignore = false
AND SendDate != ""
AND SendDate != null
AND date(SendDate) < date(now) - dur(2 weeks)
SORT date(SendDate) ASC
```

